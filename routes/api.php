<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
|                    Authentication Routes
|--------------------------------------------------------------------------
 */

Route::prefix('auth')->group(function () {

    #------------------------------ Superadmin ----------------------------#
    Route::post('/superadmin/login', 'Admin\SuperAdminAuthController@login');

    Route::middleware(['auth:sanctum'])->group(function () {
        Route::post('/logout', 'AuthController@logout');

        #------------------------------ Superadmin ----------------------------#
        Route::get('/superadmin/me/', 'Admin\SuperAdminAuthController@me');
        Route::post('/superadmin/logout', 'Admin\SuperAdminAuthController@logout');
    });

});


 // ------------------------------ auth user --------------------------------//
 Route::prefix('app')->group(function(){
    //send contact us message
    Route::post('/contact-us', 'ContactUsController@store');
    Route::get('/socialLinks', 'SettingController@retriveSocialMedia');

     Route::post('/facebook-login', 'SocialLoginController@loginWithFacebook');
     Route::post('/google-login', 'SocialLoginController@googleLogin');
     Route::post('/apple-login', 'SocialLoginController@appleLogin');
     // people profile
     Route::get('/user-profile/{username?}', 'UserController@userProfile');
     Route::middleware(['auth:sanctum'])->group(function(){
         // **********  starts user profile ********************\\
        //  Route::get('/user-profile', 'AuthController@UserMe');
         Route::post('/update-user-data', 'AuthController@updateUserTable');
         Route::post('/update-profile-privecy', 'AuthController@updatePublicOrPersonalProfile');
         Route::post('/update-current-profile', 'AuthController@updateCurrentProfile');
         Route::post('/user-logout', 'AuthController@Userlogout');
         Route::post('/update-username', 'AuthController@updateUserName');

         // *******************  end user profile  ******************* \\
         // ******************* start other users  ******************* //
        //  Route::get('/user-profile/{username?}', 'UserController@userProfile');
         Route::get('/my-profile', 'UserController@myprofile');
         Route::post('/add-new-app', 'UserController@addNewAppFromScratch');
         // ******************* end other users  ***********s******** //
         // *******************  start user personal links  ******************* \\
         Route::get('/get-single-personal-link/{id}', 'PersonalLinksController@showSinglePersonalLink');
         Route::delete('/delete-link-from-personal/{id}', 'PersonalLinksController@deleteUserLink');
         Route::post('/updated-personal-link/{id}', 'PersonalLinksController@updateLinkToUser');
         // *******************  end user personal links  ******************* \\
         // *******************  start User Links *************************** \\
         Route::get('/get-all-links', 'UserLinkController@getLinksForUser');
         Route::post('/add-user-links', 'UserLinkController@addLinksForUser');
         Route::get('/get-direct-links', 'UserLinkController@getLinksForChooseDirectLink');
         Route::post('/direct-link', 'UserLinkController@makeDirectLink');
         // ******************* start sorting apps *********************\\
         Route::post('/sort-personal-links', 'UserLinkController@updatePersonalSort');
         Route::post('/sort-business-links', 'UserLinkController@updateBusinessSort');
         // ******************* end sorting apps **********************\\
         // *******************  end   user links *************************** \\
         // ******************* Start User Business Links ********************* \\
         Route::get('/single-business-link/{id}', 'BusnisLinksController@showSingleBusinessLink');
         Route::post('/update-business-link/{id}', 'BusnisLinksController@updateBusinessLink');
         Route::delete('/delete-business-link/{id}', 'BusnisLinksController@deleteBusinessLink');
         // ******************* End User Business Links ********************** \\
         // ******************* Start Users Follow System **************************\\
         Route::post('/add-contact/{id}', 'ContactsController@addFollower');
         Route::Delete('/delete-contact/{id}', 'ContactsController@removeFollower');
         Route::get('/contacts', 'ContactsController@allFollowres');
         Route::get('/contacts-location', 'ContactsController@allFollowersLocation');
         // ******************** End User Follow System ***********************\\
    });

    // collections with apps
    Route::get('/collections/apps', 'CollectionController@getCollections');
    // apps for web site
    Route::get('/apps', 'CollectionController@getAllToPublicLinks');

     Route::prefix('setting')->group(function(){
         Route::get('/social-links', 'SettingController@retriveSocialMedia');
         Route::get('/pages/{key}', 'SettingController@getPage');
     });
 });
 //   ------------------------------ end auth user ---------------------------//


 # ------------------------------------------------------------------------ #
#                              Superadmin routes                           #
# ------------------------------------------------------------------------ #
Route::group(['middleware' => ['auth:sanctum', 'admin'], 'prefix' => 'admin'], function () {

    #---------------------------- Admin --------------------------------#
    Route::get('/', 'Admin\AdminController@index');
    Route::post('/', 'Admin\AdminController@store');
    Route::post('/update-my-profile', 'Admin\AdminController@updateProfile');
    Route::delete('/{id}', 'Admin\AdminController@remove');

    #---------------------------- Statistics --------------------------------#
    Route::get('/count', 'Admin\SettingController@count');
    Route::get('/statistics/users', 'Admin\StatisticsController@usersStatistics');

    #------------------------------- Users --------------------------------#
    Route::group(['prefix' => 'users'], function () {
        Route::get('/', 'Admin\UserController@index');
        Route::get('/{id}', 'Admin\UserController@show');
        Route::post('/{id}/block', 'Admin\UserController@block');
        Route::post('/{id}/unblock', 'Admin\UserController@unblock');
        Route::post('/{id}/delete', 'Admin\UserController@delete');

    });


    #---------------------------- vote --------------------------------#
    Route::group(['prefix' => 'collection'], function () {
        Route::get('/', 'Admin\CollectionController@getAllCollection');
        Route::post('/add', 'Admin\CollectionController@addCollection');
        Route::post('/{collection}/update', 'Admin\CollectionController@updateCollection');
        Route::delete('/{collection}/delete', 'Admin\CollectionController@deleteCollection');
    });

    Route::group(['prefix' => 'link'], function () {
        Route::get('/{id}', 'Admin\LinkController@links');
        Route::post('/add', 'Admin\LinkController@AddLink');
        Route::post('/{link}/update', 'Admin\LinkController@updateLink');
        Route::delete('/{link}/delete', 'Admin\LinkController@DeleteLink');
    });

    // Route::get('/collections', 'Admin\CollectionController@getAllCollection');
    // Route::post('/add/collection', 'Admin\CollectionController@addCollection');
    // Route::post('/update-collection/{id}', 'Admin\CollectionController@updateCollection');
    // Route::delete('/delete-collection/{collection}', 'Admin\CollectionController@deleteCollection');


    // Route::get('/all-links', 'Admin\LinkController@GetAllLinks');
    // Route::post('/add-link', 'Admin\LinkController@AddLink');
    // Route::post('/update-link/{id}', 'Admin\LinkController@updateLink');
    // Route::delete('/delete-link/{link}', 'Admin\LinkController@DeleteLink');


    #----------------------------- Settings ------------------------------#
    Route::post('/settings/{id}/update', 'Admin\SettingController@update');
    Route::get('/socialLinks', 'Admin\SettingController@retriveSocialMedia');
    Route::get('/pages/{key}', 'Admin\SettingController@getPage');
    Route::put('/pages/{key}', 'Admin\SettingController@updatePage');


    #----------------------------- Contact us ----------------------------#
    Route::group(['prefix' => 'contact-us'], function () {
        Route::get('/', 'Admin\ContactUsController@getMessages');
        Route::post('/{id}/mark-as-seen', 'Admin\ContactUsController@markAsSeen');
        Route::delete('/{id}', 'Admin\ContactUsController@remove');
    });
});


