<?php
return [
    'you-have-been-logged-in-successfully' => 'تم دخول المستخدم ',
    'added-successfully' => 'تم الأضافة',
    'updated-successfully' => 'تم التعديل',
    'deleted-successfully' => 'تم الحذف',
    'sent-successfully' => 'تم الارسال',
    'logout-successfully' => 'تم تسجيل الخروج',
    'sorry-no-result'    => 'لا يوجد نتائج',
    'sorry-you-cant-follow-your-self' => 'لا يمكنك متابعة نفسك',
    'your-direct-link-added-successfully' => 'تم اضافة الرابط المباشر الخاص بك بنجاح',
    'direct-link-removed-successfully' => 'تم حذف الرابط المباشر بنجاح',
    'sorry-it-was-added-before' => 'عفوا تمت الاضافة من قبل',
];
