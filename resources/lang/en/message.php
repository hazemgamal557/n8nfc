<?php
return [
    'you-have-been-logged-in-successfully' => 'you have been logged in successfully',
    'added-successfully' => 'Added successfully',
    'updated-successfully' => 'Updated successfully',
    'deleted-successfully' => 'Deleted successfully',
    'sent-successfully' => 'Sent successfully',
    'logout-successfully' => 'Logout successfully',
    'sorry-no-result'    => 'sorry no result',
    'sorry-you-cant-follow-your-self' => 'sorry you cant follow your self',
    'your-direct-link-added-successfully' => 'your direct link added successfully',
    'direct-link-removed-successfully' => 'direct link removed successfully',
    'sorry-it-was-added-before'  => 'sorry it was added before'

];
