<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('busnisness_links', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('business_profile_id')->index();
            $table->foreign('business_profile_id')
                ->references('id')
                ->on('business_profiles')
                ->onDelete('cascade');

            $table->unsignedBigInteger('link_id')->index();

            $table->foreign('link_id')
            ->references('id')
            ->on('links')
            ->onDelete('cascade');

            $table->string('link_url');
            $table->string('link_url_mobile')->nullable();
            $table->string('link_name')->nullable();
            $table->integer('sort');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('busnisness_links');
    }
}
