<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('facebook_id')->nullable()->unique();
            $table->string('google_id')->nullable()->unique();
            $table->string('apple_id')->nullable()->unique();
            $table->string('fullname');
            $table->string('username')->nullable()->unique();
            $table->string('email')->nullable()->unique();
            $table->timestamp('request_confirm_code_date')->nullable();
            $table->integer('confirm_code')->nullable();
            $table->boolean('active')->default(true);
            $table->text('image')->nullable();
            $table->date('birthday')->nullable();
            $table->integer('views')->default(0);
            $table->string('lang')->default('ar');
            $table->boolean('direct_link')->default(false);
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->boolean('is_public')->default(true);
            $table->string('profile_link')->nullable()->unique();
            $table->text('device_id')->nullable();
            $table->string('current_profile')->default('personal')->comment('personal|business');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
