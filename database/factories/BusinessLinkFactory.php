<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Link;
use App\BusinessProfile;
use App\BusinessLink;
use Faker\Generator as Faker;

$factory->define(BusinessLink::class, function (Faker $faker) {
    return [
        'business_profile_id' => function()
        {
            return BusinessProfile::all()->random();
        },
        'link_id' => function()
        {
            return Link::all()->random();
        },
        
        'link_url' => $faker->url
    ];
});
