<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BusinessProfile;
use App\User;
use Faker\Generator as Faker;

$factory->define(BusinessProfile::class, function (Faker $faker) {
    return [
        'user_id' => function()
        {
            return User::all()->random();
        },
        'bio' => 'welcome business'
    ];
});
