<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Link;
use App\Collection;
use Faker\Generator as Faker;

$factory->define(Link::class, function (Faker $faker) {
    return [
        'name_ar' => $faker->name,
        'name_en' => $faker->name,
        'image' => '',
        'collection_id' => function()
        {
            return Collection::all()->random();
        }
    ];
});
