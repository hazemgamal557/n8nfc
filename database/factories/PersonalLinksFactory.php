<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Link;
use App\PersonalProfile;
use App\PersonalLinks;
use Faker\Generator as Faker;

$factory->define(PersonalLinks::class, function (Faker $faker) {
    return [
        'personal_profile_id' => function()
        {
            return  PersonalProfile::all()->random();
        },

        'link_id' => function()
        {
            return Link::all()->random();
        },
        'link_url' => $faker->url,
    ];
});
