<?php

use Illuminate\Database\Seeder;

class LinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $this->seedLink();
    }
    private function seedLink()
    {
        $records = [
            [
                'id'            => 1,
                'name_en'       => 'Facebook',
                'name_ar'       => 'فيس بوك',
                'image'         => 'default/link/init/facebook.png',
                'collection_id' => 1
            ],
            [
                'id'            => 2,
                'name_en'       => 'Youtube',
                'name_ar'       => 'يوتيوب',
                'image'         => 'default/link/init/youtube.png',
                'collection_id' => 1
            ],
            [
                'id'            => 3,
                'name_en'       => 'Snapchat',
                'name_ar'       => 'سناب شات',
                'image'         => 'default/link/init/snapchat.png',
                'collection_id' => 1
            ],
            [
                'id'            => 4,
                'name_en'       => 'Instagram',
                'name_ar'       => 'انستجرام',
                'image'         => 'default/link/init/instagram.png',
                'collection_id' => 1
            ],
            [
                'id'            => 5,
                'name_en'       => 'Tiktok',
                'name_ar'       => 'تيك توك',
                'image'         => 'default/link/init/tik-tok.png',
                'collection_id' => 1
            ],
            [
                'id'            => 6,
                'name_en'       => 'Pinterest',
                'name_ar'       => 'بينتيرست',
                'image'         => 'default/link/init/pinterest.png',
                'collection_id' => 1
            ],
            [
                'id'            => 7,
                'name_en'       => 'Twitter',
                'name_ar'       => 'تويتر',
                'image'         => 'default/link/init/twitter.png',
                'collection_id' => 1
            ],
            [
                'id'            => 8,
                'name_en'       => 'Skype',
                'name_ar'       => 'سكايبى',
                'image'         => 'default/link/init/skype.png',
                'collection_id' => 1
            ],
            [
                'id'            => 9,
                'name_en'       => 'Safari',
                'name_ar'       => 'سفاري',
                'image'         => 'default/link/init/safari.png',
                'collection_id' => 1
            ],
            [
                'id'            => 10,
                'name_en'       => 'Whatsapp',
                'name_ar'       => 'واتساب',
                'image'         => 'default/link/init/whatsapp.png',
                'collection_id' => 2
            ],
            [
                'id'            => 11,
                'name_en'       => 'Message',
                'name_ar'       => 'رسائل الهاتف',
                'image'         => 'default/link/init/message.png',
                'collection_id' => 2
            ],
            [
                'id'            => 12,
                'name_en'       => 'Telegram',
                'name_ar'       => 'تيليجرام',
                'image'         => 'default/link/init/telegram.png',
                'collection_id' => 2
            ],
            [
                'id'            => 13,
                'name_en'       => 'Calls',
                'name_ar'       => 'دليل الهاتف',
                'image'         => 'default/link/init/telephone.png',
                'collection_id' => 2
            ],
        ];

        \App\Link::insert($records);
        echo " 👍\n";

    }
}
