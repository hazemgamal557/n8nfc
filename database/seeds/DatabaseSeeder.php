<?php

use Illuminate\Database\Seeder;
use App\Admin;
use App\PersonalProfile;
use App\BusinessProfile;
use App\Collection;
use App\Link;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CollectionSeeder::class,
            LinkSeeder::class,
            SettingSeeder::class
        ]);


        factory(Admin::class, 1)->create();

        factory(User::class, 20)->create()->each(function($personal){
            $personal->personalProfile()->save(factory(PersonalProfile::class)->make());
        })->each(function($business){
            $business->businessprofile()->save(factory(BusinessProfile::class)->make());
        });
    }
}
