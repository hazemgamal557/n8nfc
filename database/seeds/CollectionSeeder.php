<?php

use Illuminate\Database\Seeder;

class CollectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedCollection();
    }
    private function seedCollection()
    {
        $records = [
            [
                'id' => 1,
                'name_en' => 'Social Media',
                'name_ar' => 'تواصل اجتماعى'
            ],

            [
                'id' => 2,
                'name_en' => 'Contacts',
                'name_ar' => 'جهات الاتصال'
            ],
        ];
        \App\Collection::insert($records);
        echo " 👍\n";
    }
}
