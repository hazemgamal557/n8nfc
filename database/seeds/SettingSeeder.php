<?php

use Illuminate\Database\Seeder;
use App\Setting;
class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo "🕛 content";
        $records = [];

        $records[] = ['key' => 'page_about-us_en', 'content' => 'this is the about-us page'];
        $records[] = ['key' => 'page_privacy-policy_en', 'content' => 'this is the privacy page'];
        $records[] = ['key' => 'page_terms-of-usage_en', 'content' => 'this is the terms of usage page'];
        $records[] = ['key' => 'page_usage-policy_en', 'content' => 'this is the usage-policy page'];
        $records[] = ['key' => 'page_terms-and-conditions_en', 'content' => 'this is the terms-and-conditions page'];
        $records[] = ['key' => 'page_how-to-use_en', 'content' => 'this is the how-to-use page'];

        $records[] = ['key' => 'page_about-us_ar', 'content' => 'محتوي صفحة عن التطبيق'];
        $records[] = ['key' => 'page_privacy-policy_ar', 'content' => 'محتوى صفحة سياسة الخصوصية'];
        $records[] = ['key' => 'page_terms-of-usage_ar', 'content' => 'محتوى صفحة سياسة الاستخدام'];
        $records[] = ['key' => 'page_usage-policy_ar', 'content' => 'محتوي صفحة سياسة المستخدم'];
        $records[] = ['key' => 'page_terms-and-conditions_ar', 'content' => 'محتوي صفحة الشروط والأحكام'];
        $records[] = ['key' => 'page_how-to-use_ar', 'content' => 'محتوي صفحة كيفية الاستخدام'];

        $records[] = ['key' => 'social_facebook', 'content' => 'https://www.facebook.com/'];
        $records[] = ['key' => 'social_twitter', 'content' => 'https://www.twitter.com/'];
        $records[] = ['key' => 'social_instagram', 'content' => 'https://www.instagram.com/'];
        $records[] = ['key' => 'social_youtube', 'content' => 'https://www.youtube.com/'];
        $records[] = ['key' => 'social_whatsapp', 'content' => 'https://www.whatsapp.com/'];
        $records[] = ['key' => 'social_snapchat', 'content' => 'https://www.snapchat.com/'];
        $records[] = ['key' => 'social_phone', 'content' => '+966111111111'];
        $records[] = ['key' => 'social_gmail', 'content' => 'alexapps@gmail.com'];

        \App\Setting::insert($records);
        echo " 👍\n";
    }
}
