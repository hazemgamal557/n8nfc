<?php

namespace Tests\Feature\User;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CollectionTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function test_getCollection()
    {
        factory('App\Collection', 3)->create();
        $response = $this->json('GET', '/api/app/user/all-collections?limit=2');
        $response->assertStatus(200);
        $this->assertCount(2, json_decode($response->getContent()));
    }

    public function test_singleCollection()
    {
        $collection = factory('App\Collection')->create();
        $response = $this->json('GET', "/api/app/user/single-collection/{$collection->id}");
        $response->assertStatus(200);
        $this->assertJson($response->getContent(), true);

    }
    public function test_getAllToPublicLinks()
    {
        factory('App\Collection', 3)->create();
        factory('App\Link', 3)->create();
        $response = $this->json('GET', '/api/app/user/all-links');
        $response->assertStatus(200);
        // $this->json_decode($response->json()['data']);
        $this->assertJson($response->getContent(), true);
    }
}
