<?php

namespace Tests\Feature\User;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MessageTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function test_addMessage()
    {
        $message = [
            'name'    => 'hazem gamal',
            'email'   => 'hazemgamal55@yahoo.com',
            'message' => 'welcome popl'
        ];
        $response = $this->json('POST', '/api/app/user/add-message', $message);
        // dd($response);
        $response->assertStatus(201);
        $this->assertDatabaseHas('messages', [
            'name'    => $message['name'],
            'email'   => $message['email'],
            'message' => $message['message']
        ]);
    }
}
