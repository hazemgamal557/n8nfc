<?php

namespace Tests\Feature\User;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LinkTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    private $admin;
    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

        $this->user  = \App\User::create([
            'fullname'  => 'hazem gamal',
            'email' => 'hazemgamal55@yahoo.com',
            'username' => 'hazem123'
        ]);
        factory('App\Collection')->create();
        factory('App\PersonalProfile')->create();
        factory('App\BusinessProfile')->create();
    }
    
    public function test_getAllPersonalLinks()
    {
        factory('App\Link')->create();
        factory('App\PersonalLinks', 3)->create();
        factory('App\BusinessLink', 3)->create();

        $response = $this->actingAs($this->user)
                     ->json('GET', '/api/app/get-all-links');
        // dd($response);
        $response->assertStatus(200);
        $this->assertJson($response->getContent(), true);
    }
    
    public function test_addPersonalLinks()
    {
        $link = factory('App\Link')->create();

        $links = [
            'profile'  =>  'personal',
            'link_id'  =>  $link['id'],
            'link_url' =>  'http://www.google.com'
        ];

        $response = $this->actingAs($this->user)
                    ->json('POST', '/api/app/add-user-links', $links);
        // dd($response);
        $response->assertStatus(201);
        $this->assertDatabaseHas('personal_links', [
            'link_id'  => $links['link_id'],
            'link_url' => $links['link_url']
        ]);
    }
    public function test_addBusinessLinks()
    {
        $link = factory('App\Link')->create();

        $links = [
            'profile'  =>  'business',
            'link_id'  =>  $link['id'],
            'link_url' =>  'http://www.google.com'
        ];

        $response = $this->actingAs($this->user)
                    ->json('POST', '/api/app/add-user-links', $links);
        // dd($response);
        $response->assertStatus(201);
        $this->assertDatabaseHas('busnisness_links', [
            'link_id'  => $links['link_id'],
            'link_url' => $links['link_url']
        ]);
    }

    public function test_updatePersonalLinkToUser()
    {
         $link = factory('App\Link')->create();
         $personalLink = factory('App\PersonalLinks')->create();
         $form = [
             'link_id' => $link['id'],
             'link_url' => 'http://www.youtube.com'
         ];
         $response = $this->actingAs($this->user)
                     ->json('POST', "api/app/updated-link-from-personal/{$personalLink->id}", $form);
        $response->assertStatus(201);
        $this->assertDatabaseHas('personal_links', [
            'link_id'  => $form['link_id'],
            'link_url' => $form['link_url']
        ]);
    }

    public function test_updateBusinessLinkToUser()
    {
        $link     = factory('App\Link')->create();
        $businessLink = factory('App\BusinessLink')->create();
        $form = [
            'link_id'  => $link['id'],
            'link_url' => 'http://www.youtube.com'
        ];

        $response = $this->actingAs($this->user)
                     ->json('POST', "api/app/update-business-link/{$businessLink->id}", $form);

        $response->assertStatus(201);
        $this->assertDatabaseHas('busnisness_links', [
            'link_id'  => $form['link_id'],
            'link_url' => $form['link_url']
        ]);
    }

    public function test_getSingleBusinessLink()
    {
        factory('App\Link')->create();
        $businessLink = factory('App\BusinessLink')->create();
        $response = $this->actingAs($this->user)
                    ->json('GET', "api/app/single-business-link/{$businessLink->id}");
        // dd($response);
        $response->assertStatus(200);
        $this->assertJson($response->getContent(), true);
    }

    public function test_getSinglePersonalLink()
    {
        factory('App\Link')->create();
        $personalLink = factory('App\PersonalLinks')->create();
        $response = $this->actingAs($this->user)
                  ->json('GET', "api/app/get-single-personal-link/{$personalLink->id}");
        $response->assertStatus(200);
        $this->assertJson($response->getContent(), true);
    }

    public function test_DeletePersonalLink()
    {
        factory('App\Link')->create();
        $personalLink = factory('App\PersonalLinks')->create();
        $response = $this->actingAs($this->user)
                   ->json('DELETE', "api/app/delete-link-from-personal/{$personalLink->id}");
        $response->assertStatus(201);
        $this->assertDatabaseMissing('personal_links', [
            'link_id'   => $personalLink['link_id'],
            'link_url'  => $personalLink['link_url']
        ]);
    }

    public function test_DeleteBusinessLink()
    {
        factory('App\Link')->create();
        $businessLink = factory('App\BusinessLink')->create();
        $response = $this->actingAs($this->user)
                 ->json('DELETE', "api/app/delete-business-link/{$businessLink->id}");
        $response->assertStatus(201);
        $this->assertDatabaseMissing('busnisness_links', [
            'link_id'    => $businessLink['link_id'],
            'link_url'   => $businessLink['link_url']
        ]);
    }

}
