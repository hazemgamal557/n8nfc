<?php

namespace Tests\Feature\User;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    private $admin;
    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

        $this->user  = \App\User::create([
            'fullname'  => 'hazem gamal',
            'email' => 'hazemgamal55@yahoo.com',
        ]);
    }

    // un success case
    public function test_LoginWithFaceBook()
    {
        $auth = [
            'facebook_id' => '1957069154447275',
            'access_token'=> 'jhjhj'
        ];

        $response = $this->json('POST', '/api/app/facebook-login', $auth);
        $response->assertStatus(401);
    }

    // sucess case

    // this method is sucess test but i comment it because facebook token is leave a one houre after one houre this will be take a failor
    // public function test_SucessLoginWithFaceBook()
    // {
    //     $auth = [
    //         'facebook_id' => '1957069154447275',
    //         'access_token'=> 'EAACqvylbzKQBAGDycEiTleI1rB3eoKwzxR3hHewXDGUvaBxhZBwZCTRiKimY0LF7lzpY86jZBZAhxMnEVOWjdz3218u4IqdgO4yE6JfEEaFNGj9Af0nFATuZC1CZAZBqZCrpzF3N85hfTR0fA6EQaqDCkkWArFoRjUw9vUOTvuqawEkuMFy3vpO2kKlpiipIJfwLRIkIudbZChVXPVZBfl7Y7rrUXWKMQqZCPQ9yjvRuE4s8gZDZD'
    //     ];
    //     $response = $this->json('POST', '/api/app/facebook-login', $auth);
    //     $response->assertStatus(201);

    //     $this->assertDatabaseHas('users', [
    //         'facebook_id' => $auth['facebook_id'],
    //     ]);
    // }

    public function test_updateUserName()
    {
        $username  = [
            'username' => 'hazem123'
        ];

        $response = $this->actingAs($this->user)
                    ->json('POST', '/api/app/update-username', $username);

        $response->assertStatus(201);

        $this->assertDatabaseHas('users', [
            'username' => $username['username']
        ]);
    }

    public function test_UpdateUserProfile()
    {
        factory('App\BusinessProfile')->create();
        factory('App\PersonalProfile')->create();
        $profile = [
            'username' => 'hazem123',
            'fullname' => 'hazem gamal',
            'gender'   => 'male',
            'image'    => UploadedFile::fake()->image('0.jpg', 700, 700)->size(300),
            'bio_business' => 'welcome from business profile',
            'bio_personal'  => 'welcome from personal profile'
        ];

        $response = $this->actingAs($this->user)
                  ->json('POST', '/api/app/update-user-data', $profile);
        $response->assertStatus(201);
        $this->assertDatabaseHas('users', [
            'username' => $profile['username'],
            'fullname' => $profile['fullname'],
            'gender'   => $profile['gender'],
        ]);
    }

    public function test_userme()
    {
        $response = $this->actingAs($this->user)
                  ->json('GET', '/api/app/user-profile');

        $response->assertStatus(200);
        $this->assertJson($response->getContent(), true);
    }

    public function test_updatePrivecyProfile()
    {
        $response = $this->actingAs($this->user)
                      ->json('POST', '/api/app/update-profile-privecy');

        $response->assertStatus(201);
    }

    public function test_updateCurrentProfile()
    {
        $auth = [
            'current_profile' => 'personal'
        ];

        $response = $this->actingAs($this->user)
             ->json('POST', '/api/app/update-current-profile', $auth);

        $response->assertStatus(201);
        $this->assertDatabaseHas('users', [
            'current_profile' =>  $auth['current_profile']
        ]);
    }

}
