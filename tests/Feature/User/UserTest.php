<?php

namespace Tests\Feature\User;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    private $admin;
    private $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);

        $this->user  = \App\User::create([
            'fullname'  => 'hazem gamal',
            'email' => 'hazemgamal55@yahoo.com',
            'username' => 'hazem123'
        ]);
    }
    public function test_showUser()
    {
        $user = factory('App\User')->create();
        $response = $this->actingAs($this->user)
                     ->json('GET', "/api/app/pople-profile/{$user->username}");
        $response->assertStatus(200);
        $this->assertJson($response->getContent(), true);

    }
    public function test_myprofile()
    {
        $response = $this->actingAs($this->user)
                    ->json('GET', 'api/app/my-profile');

        $response->assertStatus(200);
        $this->assertJson($response->getContent(), true);
    }

    public function test_addmoretoPersonal()
    {
        $collection = factory('App\Collection')->create();
             factory('App\PersonalProfile')->create();
        $personal = [
            'name_en' => 'apple',
            'name_ar' => 'apple',
            'image'   => UploadedFile::fake()->image('0.jpg', 700, 700)->size(300),
            'link_url' => 'http://www.apple.com',
            'collection_id' => $collection['id'],
            'user_id'    => $this->user->id,
        ];

        $response = $this->actingAs($this->user)
                 ->json('POST', 'api/app/add-personal-app', $personal);
        $response->assertStatus(201);
        
    }

    public function test_addmoreToBusiness()
    {
        $collection = factory('App\Collection')->create();
        factory('App\BusinessProfile')->create();
        $business = [
            'name_en'       => 'mac',
            'name_ar'       => 'mac', 
            'image'         =>  UploadedFile::fake()->image('0.jpg', 700, 700)->size(300),
            'link_url'      => 'http://www.apple.com',
            'collection_id' => $collection['id'],
            'user_id'      => $this->user->id,
        ];
        $response = $this->actingAs($this->user)
                 ->json('POST', 'api/app/add-business-app', $business);
        $response->assertStatus(201);
    }
}
