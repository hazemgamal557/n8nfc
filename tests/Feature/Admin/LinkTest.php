<?php

namespace Tests\Feature\Admin;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LinkTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    private $admin;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);
    }

    public function test_GetAllLinks()
    {
        factory('App\Collection', 3)->create();
        factory('App\Link', 3)->create();

        $response = $this->actingAs($this->admin)
                    ->json('GET', '/api/admin/all-links?limit=2');
        $response->assertStatus(200);
        $this->assertCount(2, $response->json()['data']);
    }

    public function test_addLink()
    {
        $collection = factory('App\Collection')->create();
        $link = [
            'name_ar' => 'whatsapp',
            'name_en' => 'whatsapp',
            'image'   => UploadedFile::fake()->image('0.jpg', 700, 700)->size(300),
            'collection_id' => $collection['id'],
        ];

        $response = $this->actingAs($this->admin)
                      ->json('POST', '/api/admin/add-link', $link);
        $response->assertStatus(201);
        $this->assertDatabaseHas('links', [
            'name_ar' => $link['name_ar'],
            'name_en' => $link['name_en'],
        ]);

    }

    public function test_singleLink()
    {
        factory('App\Collection')->create();
        $link = factory('App\Link')->create();

        $response = $this->actingAs($this->admin)
                    ->json('GET', "/api/admin/single-link/{$link->id}");
        $response->assertStatus(200);

        $this->assertJson($response->getContent(), true);
    }

    public function test_deleteLink()
    {
        factory('App\Collection')->create();
        $link = factory('App\Link')->create();
        $response = $this->actingAs($this->admin)
                        ->json('DELETE', "/api/admin/delete-link/{$link->id}");
        $response->assertStatus(201);

        $this->assertDatabaseMissing('links', [
            'id' => $link->id
        ]);
    }

    public function test_UpdateLink()
    {
        $collection = factory('App\Collection')->create();
        $link = factory('App\Link')->create();
        $form  = [
            'name_ar' => 'whatsapp',
            'name_en' => 'whatsapp',
            'image'   => UploadedFile::fake()->image('0.jpg', 700, 700)->size(300),
            'collection_id' => $collection['id'],
        ];

        $response = $this->actingAs($this->admin)
                    ->json('POST', "/api/admin/update-link/{$link->id}", $form);
        $response->assertStatus(201);

        $this->assertDatabaseHas('links', [
            'name_ar' => $form['name_ar'],
            'name_en' => $form['name_en'],
        ]);
    }

}
