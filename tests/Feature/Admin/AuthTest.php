<?php

namespace Tests\Feature\Admin;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);
    }

    public function test_login()
    {
        $auth = [
            'email' => 'admin@admin.com',
            'password' => 'secret'
        ];

        $response = $this->json('POST', '/api/admin/login', $auth);
        $response->assertStatus(201);

        $record = json_decode($response->getContent(), true);

        $this->assertDatabaseHas('admins', [
            'email' => $auth['email'],
        ]);

    }

    public function test_adminMe()
    {
        $response = $this->actingAs($this->admin)
                      ->json('GET', '/api/admin/admin-profile');
        $response->assertStatus(200);

        $record = json_decode($response->getContent(), true);
        $this->assertDatabaseHas('admins', [
            'email' => 'admin@admin.com',
        ]);
    }

    public function test_logout()
    {
        $response = $this->actingAs($this->admin)
                       ->json('POST', '/api/admin/admin-logout');
        $response->assertStatus(200);
        $record = json_decode($response->getContent(), true);

        $this->assertDatabaseHas('admins', [
            'email' => 'admin@admin.com',
        ]);
    }


}
