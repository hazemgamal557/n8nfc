<?php

namespace Tests\Feature\Admin;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CollectionTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    private $admin;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = \App\Admin::create([
            'name'     => 'أدمن',
            'email'    => 'admin@admin.com',
            'password' => \Hash::make('secret')
        ]);
    }

    public function test_retriveCollection()
    {
        factory('App\Collection', 3)->create();
        $response = $this->actingAs($this->admin)
                     ->json('GET', '/api/admin/collections?limit=2');
        $response->assertStatus(200);
        $this->assertCount(2, $response->json()['data']);
    }

    public function test_singleCollection()
    {
        $collection = factory('App\Collection')->create();

        $response = $this->actingAs($this->admin)
                         ->json('GET', "api/admin/collection/{$collection->id}");
        $response->assertStatus(200);

        $this->assertJson($response->getContent(), true);
    }

    public function test_addCollection()
    {
        $collection = [
            'name_en' => 'whatsapp',
            'name_ar' => 'واتس اب'
        ];
        $response = $this->actingAs($this->admin)
                   ->json('POST', '/api/admin/add/collection', $collection);
        $response->assertStatus(201);
        $this->assertDatabaseHas('collections', [
            'name_en' => $collection['name_en'],
            'name_ar' => $collection['name_ar']
        ]);
    }

    public function test_updateCollection()
    {
        $collection = factory('App\Collection')->create();
        $form = [
            'name_en' => 'faceBook',
            'name_ar' => 'فيس بوك',
        ];
        $response = $this->actingAs($this->admin)
                    ->json('POST', "/api/admin/update-collection/{$collection->id}", $form);
        $response->assertStatus(201);
        $this->assertDatabaseHas('collections', [
            'name_ar' => $form['name_ar'],
            'name_en' => $form['name_en']
        ]);
    }

    public function test_DeleteCollection()
    {
        $collection = factory('App\Collection')->create();
        $response = $this->actingAs($this->admin)
                    ->json('DELETE', "/api/admin/delete-collection/{$collection->id}");
        $response->assertStatus(201);
        $this->assertDatabaseMissing('collections', [
            'id' => $collection->id,
            'name_ar' => $collection->name_ar,
            'name_en' => $collection->name_en
        ]);

    }
}
