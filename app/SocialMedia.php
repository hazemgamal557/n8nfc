<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    protected $fillable = [
        'facebook',
        'twitter',
        'snapchat',
        'instagram',
        'youtube',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
