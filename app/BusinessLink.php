<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessLink extends Model
{

    protected $table = 'busnisness_links';

    protected $fillable = [
        'business_profile_id',
        'link_id',
        'link_url',
        'link_url_mobile',
        'link_name',
        'sort'
    ];

    public function setLinkUrlAttribute($value)
    {
        $this->attributes['link_url'] = strpos($value, '://') !== false || empty($this->link->prefix)
            ? $value
            : str_replace(
                ['#', '\\', '$', '^', ' '],
                '',
                str_replace('(.*)', $value, $this->link->prefix)
            );
    }

    public function setLinkUrlMobileAttribute($value)
    {
        $this->attributes['link_url_mobile'] = strpos($value, '://') !== false || empty($this->link->prefix_mobile)
            ? $value
            : str_replace(
                ['#', '\\', '$', '^', ' '],
                '',
                str_replace('(.*)', $value, $this->link->prefix_mobile)
            );
    }

    public function getLinkUsernameAttribute() : string
    {
        if (empty($this->link->prefix)) return '';

        $matches = [];
        preg_match($this->link->prefix, $this->attributes['link_url'], $matches);
        return $matches[1] ?? '';
    }

    public function businessProfile()
    {
        return $this->belongsTo(BusinessProfile::class);
    }

    public function link()
    {
        return $this->belongsTo(Link::class);
    }
}
