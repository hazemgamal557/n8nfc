<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OtherUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $user = [
            'id' => $this->id,
            'fullname' => $this->fullname,
            'username' => $this->username,
            'email' => $this->email,
            'image' => $this->image,
            'views' => $this->views,
            'gender' => $this->gender,
            'is_public' => (bool) $this->is_public,
            'direct_link' => (bool) $this->direct_link,
            'current_profile' => $this->current_profile,
            'profile_link' => 'https://nas8me.co/' . $this->username,
        ];
        if ($this->current_profile == 'personal') {
            $user['bio'] = $this->personalProfile->bio;
            $user['links'] = PersonalLinkResource::collection($this->personalProfile->personalLinks);
        } elseif ($this->current_profile == 'business') {
            $user['bio'] = $this->businessprofile->bio;
            $user['links'] = BusinessLinkResource::collection($this->businessprofile->businessLinks);
        }

        return $user;
    }
}
