<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MiniUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $user = [
            'id'       => $this->id,
            'fullname' => $this->fullname,
            'username' => $this->username,
            'views'    => $this->views,
            'image'    => $this->image,
            'has_direct_link' => (bool) $this->direct_link,
            'direct_link'   => $this->direct_link ? $this->current_direct_link : null ,
            'direct_link_mobile'   => $this->direct_link ? $this->current_direct_link_mobile : null ,
        ];
        return $user;
        
    }
}
