<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BusinessLinkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'link_id'      => $this->id,
            'url'  => $this->link_url,
            'url_mobile'  => $this->link_url_mobile,
            'link_username' => $this->link_username,
            'name' => $this->link_name ?? '',
            'app' => new LinkResource($this->link),

        ];
    }
}
