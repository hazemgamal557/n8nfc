<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $user = [
            'id' => $this->id,
            'fullname' => $this->fullname,
            'username' => $this->username,
            'email' => $this->email,
            'image' => $this->image,
            'views' => $this->views,
            'gender' => $this->gender,
            'birthday' => $this->birthday,
            'is_public' => (bool) $this->is_public,
            'direct_link' => (bool) $this->direct_link,
            'current_profile' => $this->current_profile,
            'personal_bio' => $this->personalProfile->bio,
            'business_bio' => $this->businessprofile->bio,
            'profile_link' => 'nas8me.co/' . $this->username
        ];
        return $user;
    }
}
