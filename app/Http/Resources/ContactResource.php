<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $followers = [
            'id' => $this->id,
            'latitude' => strval($this->latitude),
            'longitude' => strval($this->longitude),
            'location' => "https://maps.google.com/?q={$this->latitude},{$this->longitude}",
            // 'user' => new OtherUserResource($this->userFollowed)
            'user'   => new MiniUserResource($this->userFollowed)
            // 'is_followed' => (bool) $this->is_followed == true ? true : false

        ];

        // if($this->relationLoaded('userFollowed'))
        // {
        //     // $followers['user_id'] = $this->userFollowed['personalProfile']['id'];
        //     // $followers['user_id'] = $this->userFollowed['personalProfile']['id'];
        //     // $followers['facebook_id'] = $this->userFollowed['facebook_id'];
        //     $followers['fullname'] = $this->userFollowed['fullname'];
        //     // temperary
        //     $followers['username'] = $this->userFollowed['username'];

        //     $followers['image'] = $this->userFollowed['image'];
        //     $followers['views'] = $this->userFollowed['views'];
        // }

        return $followers;

    }
}
