<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserLocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        $followers = [
            'id'          => $this->id,
            'latitude'    => strval($this->latitude),
            'longitude'   => strval($this->longitude),
            'username'    => $this->userFollowed->username,
            'views'       => $this->userFollowed->views,
            'fullname'    => $this->userFollowed->fullname,
            'navigation'  => 'https://www.google.com/maps/dir/?api=1&origin=Current+Location&destination='.$this->latitude.','.$this->longitude.'&travelmode=driving'
        ];

        return $followers;
    }
}
