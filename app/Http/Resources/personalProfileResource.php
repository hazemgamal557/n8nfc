<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class personalProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            // 'id' => $this->id,
            'personal_bio' => $this->bio,

        ];
    }
}
