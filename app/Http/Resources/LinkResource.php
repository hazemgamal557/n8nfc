<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LinkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'name' => $this->{'name_' . app()->getLocale()},
            'image' => $this->image,
            'used_count' => ($this->personal_links_count ?? 0) + ($this->business_links_count ?? 0),
            'has_username' => (bool) $this->prefix != '#^(.*)$#'
        ];
    }
}
