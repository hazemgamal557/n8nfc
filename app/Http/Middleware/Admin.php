<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth::user();
        if($user->tokenCan('server:update')){

            return $next($request);
        }else{
            return response()->json([
                'message' => 'sorry you are not authorized'
            ], 401);
        }
    }
}
