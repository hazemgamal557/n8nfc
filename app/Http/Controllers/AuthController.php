<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Traits\UpdateProfileTrait;
use App\Http\Resources\UserResource as UserResource;
use App\PersonalProfile;
use App\BusinessProfile;
use App\Services\FileManager;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AuthController extends Controller
{
    use UpdateProfileTrait;
    public function registerAndSendEmail(Request $request, FileManager $fileManager)
    {
        $data = $request->validate([
            'email' => [
                'required',
                'email',
                Rule::unique('users')->where('email_verified_at', '!=', null),
            ],
            'name' => 'required|string',
            'password' => 'required',
            'image' => 'image|mimes:jpeg,jpg,png',
        ]);

        User::where('email', $data['email'])
            ->where('email_verified_at', null)
            ->delete();

        if ($request->hasFile('image') && request()->file('image')->isValid()) {
            $data['image'] = $fileManager->uploadUserImage(request()->file('image'));
        }
        $data['password'] = \Hash::make($data['password']);
        $user = User::create($data);
        $user->email_verified_at = now();
        $user->save();

        // business profile

        $businessProfile = new BusinessProfile;
        $businessProfile->user_id = $user->id;
        $businessProfile->save();

        // personal profile
        $personalProfile = new PersonalProfile;
        $personalProfile->user_id = $user->id;
        $personalProfile->save();

        return response()->json([
            'user' => new UserResource($user),
            'token' => $user->createToken('user-token', ['server:show'])->plainTextToken,
            'message' => 'signup success',
        ], 201);

    }

    public function userLogin(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        $user = User::where('email', $data['email'])
            ->where('email_verified_at', '!=', null)
            ->first();
        if(is_null($user) || !\Hash::check($data['password'], $user->password))
        {
            return response()->json([
                'message' => 'sorry email or password not right'
            ], 401);
        }
        return response()->json([
            'user' => new UserResource($user),
            'token' => $user->createToken('user-token', ['server:show'])->plainTextToken,
            'message' => 'logoin successfully'
        ], 201);
    }
    public function UserMe(Request $request)
    {
        $user = \Auth::user();
        return new UserResource($user);
    }

    public function Userlogout(Request $request)
    {
        $request->user()->tokens()->delete();
        return response()->json([
            'message' => 'logout-successfully'
        ], 201);


    }


    // protected function generateConfirmationCode(): string
    // {
    //     return (string) 111111;
    // }

}


