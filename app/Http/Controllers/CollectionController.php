<?php

namespace App\Http\Controllers;

use App\Collection;
use App\Http\Resources\CollectionResource;
use App\Http\Resources\LinkResource;
use App\Link;
use App\PersonalLinks;
use App\BusinessLink;
use Illuminate\Http\Request;

class CollectionController extends Controller
{
    public function getCollections(Request $request)
    {
        $collections = Collection::orderBy('id', 'asc')
            ->with(['links' => function ($query) {
                $query->where('user_id', null);

                $authGuard = \Auth::guard('sanctum');
                if($authGuard->check()) {
                    $query->withCount([
                        'personalLinks' => function ($query) use ($authGuard) {
                            $query->where('personal_profile_id', $authGuard->user()->personalProfile->id);
                        },
                        'businessLinks' => function ($query) use ($authGuard) {
                            $query->where('business_profile_id', $authGuard->user()->businessprofile->id);
                        }
                    ]);
                }
            }]);

        $records = CollectionResource::collection($collections->get());
        return response()->json($records);
    }

    public function getAllToPublicLinks(Request $request)
    {
        $links = Link::orderBy('id', 'asc')->where('user_id', null)->get();

        $records = LinkResource::collection($links);
        return response()->json($records);
    }
}
