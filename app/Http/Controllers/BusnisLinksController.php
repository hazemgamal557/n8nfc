<?php

namespace App\Http\Controllers;

use App\BusinessLink;
use App\BusinessProfile;
use App\Http\Resources\BusinessLinkResource;
use Illuminate\Http\Request;

class BusnisLinksController extends Controller
{

    public function showSingleBusinessLink(Request $request, $id)
    {
        $businessProfile = BusinessProfile::where('user_id', \Auth::user()->id)
            ->first();
        $businessLink = BusinessLink::where('business_profile_id', $businessProfile->id)
            ->findOrFail($id);

        $records = new BusinessLinkResource($businessLink);

        return response()->json($records);
    }

    public function updateBusinessLink(Request $request, $id)
    {
        $data = $request->validate([
            'link_id' => 'integer|exists:links,id|nullable',
            'link_url' => 'string|nullable',
            'link_name' => 'string|nullable'
        ]);

        $businessProfile = BusinessProfile::where('user_id', \Auth::user()->id)
            ->first();
        $businessLink = BusinessLink::where('business_profile_id', $businessProfile->id)
            ->findOrFail($id);

        if (!empty($data['link_id'])) {
            $businessLink->link_id = $data['link_id'];
        }

        if (!empty($data['link_url'])) {
            $businessLink->link_url = $data['link_url'];
            $businessLink->link_url_mobile = $data['link_url'];
        }

        if (!empty($data['link_name'])) {
            $businessLink->link_name = $data['link_name'];
        }

        $businessLink->save();

        return response()->json([
            'message' => __('message.updated-successfully'),

        ], 201);
    }

    public function deleteBusinessLink($id)
    {
        $businessProfile = BusinessProfile::where('user_id', \Auth::user()->id)
            ->first();
        $businessLink = BusinessLink::where('business_profile_id', $businessProfile->id)
            ->findOrFail($id);
        $businessLink->delete();
        return response()->json([
            'message' => __('message.deleted-successfully'),
        ], 201);
    }
}
