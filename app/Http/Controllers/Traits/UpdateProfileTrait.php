<?php
namespace App\Http\Controllers\Traits;

use App\User;
use App\PersonalProfile;
use App\BusinessProfile;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\Services\FileManager;

trait UpdateProfileTrait
{
    public function updateUserTable(Request $request, FileManager $fileManager)
    {
        $data = $request->validate([
            'username' => [
                'string',
                'min:2',
                'regex:/^[A-Za-z][A-Za-z0-9]{2,30}$/',
                Rule::unique('users')->ignore(\Auth::id()),
                'nullable'
            ],
            'fullname'        => 'string|nullable',
            'gender'          => [
                Rule::in(['male', 'female']),
                'nullable'
            ],
            'image'           => 'image|mimes:jpeg,jpg,png|nullable',
            'current_profile' => 'string|nullable',

            // 'bio_is_personal'    => 'boolean|nullable',
            // 'bio'               => 'string|nullable',
            'bio_personal'    => 'string|nullable',
            'bio_business'    => 'string|nullable',
            'is_public'       => 'boolean|nullable',
            'profile_link'    => [
                'url',
                Rule::unique('users')->ignore(\Auth::id()),
                'nullable'

            ],
            'current_profile' => [
                Rule::in(['personal', 'business']),
                'nullable'
            ],
            'birthday' => 'date_format:Y-m-d'
        ]);
        $user = \Auth::user();
        if(is_null($user))
        {
            return response()->json([
                'message' => 'sorry you are not authourized'
            ], 401);
        }

        if ($request->hasFile('image') && request()->file('image')->isValid()) {
            $user->image = $fileManager->uploadUserImage(request()->file('image'));
        }
        // if(isset($data['bio_is_personal']) && !empty($data['bio']))
        // {
        //     $profile = $data['bio_is_personal'] == true
        //         ? PersonalProfile::where('user_id', $user->id)->first()
        //         : BusinessProfile::where('user_id', $user->id)->first();

        //     $profile->bio = $data['bio'];
        //     $profile->save();
        // }

        if(!empty($data['bio_personal']))
        {
            $personalProfile = PersonalProfile::where('user_id', $user->id)->first();
            $personalProfile->bio = $data['bio_personal'];
            $personalProfile->save();
        }

        if(!empty($data['bio_business']))
        {
            $businessProfile = BusinessProfile::where('user_id', $user->id)->first();
            $businessProfile->bio = $data['bio_business'];
            $businessProfile->save();
        }

        if(!empty($data['fullname']))
        {
            $user->fullname = $data['fullname'];
        }
        if(!empty($data['gender']))
        {
            $user->gender = $data['gender'];
        }


        if(!empty($data['username']))
        {
            $user->username = $data['username'];
        }

        if(isset($data['is_public']))
        {
            $user->is_public = $data['is_public'];

        }
        if(!empty($data['current_profile']))
        {
            $user->current_profile = $data['current_profile'];
        }
        if(!empty($data['birthday']))
        {
            $user->birthday = $data['birthday'];
        }
        if(!empty($data['profile_link']))
        {
            $user->profile_link = $data['profile_link'];
        }
        $user->save();

        return response()->json([
            'message' => __('message.updated-successfully')
        ], 201);
    }
    public function updatePublicOrPersonalProfile(Request $request)
    {
        $user = \Auth::user();
        if($user->is_public == true)
        {
            $user->is_public = false;
            $user->save();
            return response()->json([
                'message' => 'your profile is private now'
            ], 201);
        }else if($user->is_public == false)
        {
            $user->is_public = true;
            $user->save();
            return response()->json([
                'message' => 'your profile is public now'
            ], 201);
        }
    }

    public function updateCurrentProfile(Request $request)
    {
        $data = $request->validate([
            'current_profile' => 'string|nullable'
        ]);
        $user = \Auth::user();
        if(!empty($data['current_profile']))
        {
            if($data['current_profile'] == 'personal')
            {
                $user->current_profile = $data['current_profile'];
                $user->save();
                return response()->json([
                    'message' => 'your current profile is a personal now'
                ], 201);
            }else if($data['current_profile'] == 'business')
            {
                $user->current_profile = $data['current_profile'];
                $user->save();
                return response()->json([
                    'message' => 'your current profile is a business now'
                ], 201);
            }else{
                return response()->json([
                    'message' => 'sorry you should enter a personal or public profile'
                ], 401);
            }
        }

    }
}
