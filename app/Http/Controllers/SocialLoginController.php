<?php

namespace App\Http\Controllers;

use App\BusinessProfile;
use App\Http\Resources\UserResource as UserResource;
use App\PersonalProfile;
use App\Services\FileManager;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SocialLoginController extends Controller
{
    public function loginWithFacebook(Request $request)
    {
        $facebookData = $request->validate([
            'facebook_id' => 'required|string',
            'access_token' => 'required',

        ]);
        $firstTimeLogin = false;

        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->get("https://graph.facebook.com/" . $facebookData['facebook_id'] .
                "?fields=id,name,email,birthday,gender&access_token=" . $facebookData['access_token']);
        } catch (\Exception $e) {
            return response()->json([
                "message" => 'Invalid token',
            ], 401);
        }

        $data = json_decode($response->getBody(), true);

        if (empty($data['email'])) {

            // (LOGIN)
            $user = User::where('facebook_id', $data['id'])
                ->first();

            // (SIGN UP)
            if (is_null($user)) {

                $user = new user();
                $user->facebook_id = $data['id'];
                $user->active = 1;
                $user->is_public = 1;
                $user->views = 0;

                if (!empty($data['name'])) {
                   $user->fullname = $data['name'];
                }
                if (!empty($data['gender'])) {
                    $user->gender = $data['gender'];
                 }
                 if (!empty($data['email'])) {
                    $user->email = $data['email'];
                 }
                 if (!empty($data['birthday'])) {
                    $user->birthday = now()->parse($data['birthday'])->toDateString();
                }

                $user->save();

                // personal profile
                $personalProfile = new PersonalProfile;
                $personalProfile->user_id = $user->id;
                $personalProfile->save();

                // business profile
                $businessProfile = new BusinessProfile;
                $businessProfile->user_id = $user->id;
                $businessProfile->save();
                $firstTimeLogin = true;
            }
        } else {
            // (LOGIN)
            $user = User::where('email', $data['email'])
                ->first();

            if ($user && !$user->facebook_id) {
                $user->facebook_id = $data['id'];
                $user->update();
            }

            // (SIGN UP)
            if (is_null($user)) {

                $user = new user();
                $user->email = $data['email'];
                $user->is_public = 1;
                $user->active = 1;
                $user->views = 0;
                $user->facebook_id = $data['id'];

                if (!empty($data['name'])) {
                   $user->fullname = $data['name'];
                }
                if (!empty($data['gender'])) {
                    $user->gender = $data['gender'];
                 }

                 if (!empty($data['birthday'])) {
                    $user->birthday = now()->parse($data['birthday'])->toDateString();
                }


                $user->save();

                 // personal profile
                $personalProfile = new PersonalProfile;
                $personalProfile->user_id = $user->id;
                $personalProfile->save();

                // business profile
                $businessProfile = new BusinessProfile;
                $businessProfile->user_id = $user->id;
                $businessProfile->save();
                $firstTimeLogin = true;
            }
        }

        if ($user->active == 0) {
            return response()->json([
                "message" => 'not-active',
            ], 401);
        }

        return response()->json([
            'firstTimeLogin' => $firstTimeLogin,
            'user' => new UserResource($user),
            'token' => $user->createToken('user-token', ['server:show'])->plainTextToken,
            'message' => __('message.you-have-been-logged-in-successfully'),
        ], 201);
    }

    public function googleLogin(Request $request, FileManager $fileManager)
    {
        $requestedData = $request->validate([
            'access_token' => 'required',
            'name' => 'required|string',
            'image' => 'string|nulla ble',
        ]);
        $firstTimeLogin = false;
        // validate the requested access token
        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->get("https://oauth2.googleapis.com/tokeninfo?access_token="
                . $requestedData['access_token']);
        } catch (\Exception $e) {
            return response()->json([
                "message" => 'Invalid token',
            ], 401);
        }

        // store the data sent by google
        $data = json_decode($response->getBody(), true);

        // check the user exists or create
        $user = User::where('email', $data['email'])->first();

        if ($user && !$user->google_id) {
            $user->google_id = $data['sub'];
            $user->update();
        }
        if (is_null($user)) {
            $user = new user();
            $user->google_id = $data['sub'];
            if(!empty($data['gender']))
            {
                $user->gender = $data['gender'];
            }
            $user->fullname = $requestedData['name'];
            $user->is_public = 1;
            $user->active = 1;
            $user->views = 0;
            $user->email = $data['email'];
            if (!empty($data['birthday'])) {
                $user->birthday = now()->parse($data['birthday'])->toDateString();
            }

            if ($request->hasFile('image') && request()->file('image')->isValid()) {
                $user->image = $fileManager->uploadUserImage(request()->file('image'));
            }
            $user->save();

            // personal profile
            $personalProfile = new PersonalProfile;
            $personalProfile->user_id = $user->id;
            $personalProfile->save();

            // business profile
            $businessProfile = new BusinessProfile;
            $businessProfile->user_id = $user->id;
            $businessProfile->save();
            $firstTimeLogin = true;
        }

        if ($user->active == 0) {
            return response()->json([
                "message" => 'not-active',
            ], 401);
        }

        return response()->json([
            'firstTimeLogin' => $firstTimeLogin,
            'user' => new UserResource($user),
            'token' => $user->createToken('user_token', ['server:show'])->plainTextToken,
            'message' => __('message.you-have-been-logged-in-successfully'),
        ], 201);

    }
    public function appleLogin(Request $request)
    {
        $requestedData = $request->validate([
            'id_token' => 'required',
        ]);
        $firstTimeLogin = false;
        try {
            \SocialiteProviders\Apple\Provider::verify($requestedData['id_token']);

        } catch (\Exception $e) {
            return response()->json([
                "message" => 'Invalid token',
            ], 401);
        }

        $claims = explode('.', $requestedData['id_token'])[1];

        $data = json_decode(base64_decode($claims), true);

        $name = strtok($data['email'], '@');

        // (LOGIN)
        $user = User::where('email', $data['email'])
            ->first();

        if ($user && !$user->apple_id) {
            $user->apple_id = $data['sub'];
            $user->update();
        }

        // (SIGN UP)
        if (is_null($user)) {

            $user = new user();
            $user->email = $data['email'];
            $user->apple_id = $data['sub'];
           $user->fullname = $name;
            $user->is_public = 1;
            $user->active = 1;
            $user->views = 0;
            if (!empty($data['birthday'])) {
                $user->birthday = now()->parse($data['birthday'])->toDateString();
            }

            $user->save();

            // personal profile
            $personalProfile = new PersonalProfile;
            $personalProfile->user_id = $user->id;
            $personalProfile->save();

            // business profile
            $businessProfile = new BusinessProfile;
            $businessProfile->user_id = $user->id;
            $businessProfile->save();
            $firstTimeLogin = true;
        }

        if ($user->active == 0) {
            return response()->json([
                "message" => 'not-active',
            ], 401);
        }

        return response()->json([
            'firstTimeLogin' => $firstTimeLogin,
            'user' => new UserResource($user),
            'token' => $user->createToken('user_token', ['server:show'])->plainTextToken,
            'message' => __('message.you-have-been-logged-in-successfully'),
        ], 201);

    }
}
