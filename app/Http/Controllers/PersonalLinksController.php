<?php

namespace App\Http\Controllers;

use App\Link;
use App\PersonalLinks;
use App\PersonalProfile;
use App\Http\Resources\PersonalLinkResource;
use Illuminate\Http\Request;

class PersonalLinksController extends Controller
{
    public function showSinglePersonalLink(Request $request, $id)
    {
        $personalProfile = PersonalProfile::where('user_id', \Auth::user()->id)->first();
        $singlePersonalLink = PersonalLinks::where('personal_profile_id', $personalProfile->id)
        ->findOrFail($id);

        $records = new PersonalLinkResource($singlePersonalLink);

        return response()->json($records);
    }

    public function updateLinkToUser(Request $request, $id)
    {
        $data = $request->validate([
            'link_id' => 'integer|exists:links,id|nullable',
            'link_url' => 'string|nullable',
            'link_name' => 'string|nullable'
        ]);

        $personalprofile = PersonalProfile::where('user_id', \Auth::user()->id)->first();
        $personalLink = PersonalLinks::where('personal_profile_id', $personalprofile->id)
            ->findOrFail($id);

        if (!empty($data['link_id'])) {
            $personalLink->link_id = $data['link_id'];
        }

        if (!empty($data['link_url'])) {
            $personalLink->link_url = $data['link_url'];
            $personalLink->link_url_mobile = $data['link_url'];
        }

        if (!empty($data['link_name'])) {
            $personalLink->link_name = $data['link_name'];
        }
        $personalLink->save();
        return response()->json([
            'message' => __('message.updated-successfully'),
        ], 201);

    }

    public function deleteUserLink(Request $request, $id)
    {
        $personalprofile = PersonalProfile::where('user_id', \Auth::user()->id)->first();
        $personalLink = PersonalLinks::where('personal_profile_id', $personalprofile->id)
            ->findOrFail($id);

        $personalLink->delete();

        return response()->json([
            'message' => __('message.deleted-successfully'),
        ], 201);
    }
    
}
