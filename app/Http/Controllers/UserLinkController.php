<?php

namespace App\Http\Controllers;

use App\Link;
use App\BusinessLink;
use App\BusinessProfile;
use App\Http\Resources\BusinessLinkResource;
use App\Http\Resources\PersonalLinkResource;
use App\PersonalLinks;
use App\PersonalProfile;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserLinkController extends Controller
{
    public function getLinksForUser(Request $request)
    {
        // get personal links
        $personalProfile = PersonalProfile::where('user_id', \Auth::user()->id)->first();
        $personalLinks = PersonalLinks::where('personal_profile_id', $personalProfile->id)->orderBy('sort', 'asc')->get();
        $personalRecords = PersonalLinkResource::collection($personalLinks);
        // get business links

        $businessProfile = BusinessProfile::where('user_id', \Auth::user()->id)->first();
        $businessLinks = BusinessLink::where('business_profile_id', $businessProfile->id)->orderBy('sort', 'asc')->get();
        $businessRecords = BusinessLinkResource::collection($businessLinks);

        return response()->json([
            'personal_links' => $personalRecords,
            'business_links' => $businessRecords,
        ]);
    }

    public function addLinksForUser(Request $request)
    {
        $data = $request->validate([
            'profile' => 'required|string',
            'link_id' => 'required|integer|exists:links,id',
            'link_url' => 'required|string',
        ]);

        if ($data['profile'] == 'personal') {
            $personalProfile = PersonalProfile::where('user_id', \Auth::user()->id)->first();

            $personalLinks = new PersonalLinks;
            $personalLinks->personal_profile_id = $personalProfile->id;
            $personalLinks->link_id = $data['link_id'];

            $personalLinks->link_url = $data['link_url'];
            $personalLinks->link_url_mobile = $data['link_url'];

            $sortPersoanlLink = PersonalLinks::where('personal_profile_id', $personalProfile->id)
                ->orderBy('sort', 'desc')->first();

            if (!is_null($sortPersoanlLink)) {
                $personalLinks->sort = $sortPersoanlLink->sort + 1;

            } else {
                $personalLinks->sort = 1;

            }

            $personalLinks->save();

            return response()->json([
                'message' => __('message.added-successfully'),
            ], 201);
        } elseif ($data['profile'] == 'business') {
            $businessProfile = BusinessProfile::where('user_id', \Auth::user()->id)->first();

            $businessLinks = new BusinessLink;
            $businessLinks->business_profile_id = $businessProfile->id;
            $businessLinks->link_id = $data['link_id'];
            $businessLinks->link_url = $data['link_url'];
            $businessLinks->link_url_mobile = $data['link_url'];

            $sortBusinessLinks = BusinessLink::where('business_profile_id', $businessProfile->id)
                ->orderBy('id', 'desc')->first();

            if (!is_null($sortBusinessLinks)) {
                $businessLinks->sort = $sortBusinessLinks->sort + 1;

            } else {
                $businessLinks->sort = 1;
            }

            $businessLinks->save();

            return response()->json([
                'message' => __('message.added-successfully'),
            ], 201);
        } else {
            return response()->json([
                'message' => 'sorry your profile must be personal or business ',
            ], 404);
        }
    }

    public function getLinksForChooseDirectLink(Request $request)
    {
        $data = $request->validate([
            'profile' => [
                'required',
                'string',
                Rule::in(['personal', 'business']),
            ],
        ]);

        if ($data['profile'] == 'personal') {
            // get personal links
            $personalProfile = PersonalProfile::where('user_id', \Auth::user()->id)->first();
            $personalLinks = PersonalLinks::where('personal_profile_id', $personalProfile->id)
                ->orderBy('sort', 'asc')
                ->get();
            $records = PersonalLinkResource::collection($personalLinks);
        } else {

            $businessProfile = BusinessProfile::where('user_id', \Auth::user()->id)->first();
            $businessLinks = BusinessLink::where('business_profile_id', $businessProfile->id)
                ->orderBy('sort', 'asc')
                ->get();
            $records = BusinessLinkResource::collection($businessLinks);
        }

        return response()->json([
            'links' => $records,
        ]);

    }

    public function updatePersonalSort(Request $request)
    {

        $this->validate($request, [
            '*.id' => 'required|numeric|exists:personal_links,id',
            '*.sort' => 'required|numeric',
        ]);

        $personalProfile = PersonalProfile::where('user_id', \Auth::user()->id)
            ->first();

        $data = ['data' => $request->all()];

        foreach ($data['data'] as $r) {
            PersonalLinks::where('personal_profile_id', $personalProfile->id)
                ->where('id', $r['id'])
                ->update(['sort' => $r['sort']]);
        }

        return response()->json([
            'message' => __('message.updated-successfully'),
        ], 201);
    }

    public function updateBusinessSort(Request $request)
    {

        $this->validate($request, [
            '*.id' => 'required|numeric|exists:busnisness_links,id',
            '*.sort' => 'required|numeric',
        ]);

        $businessProfile = BusinessProfile::where('user_id', \Auth::user()->id)
            ->first();

        $data = ['data' => $request->all()];

        foreach ($data['data'] as $r) {
            BusinessLink::where('business_profile_id', $businessProfile->id)
                ->where('id', $r['id'])
                ->update(['sort' => $r['sort']]);
        }

        return response()->json([
            'message' => __('message.updated-successfully'),
        ], 201);
    }

    public function makeDirectLink(Request $request)
    {
        $user = \Auth::user();
        $user->direct_link = !$user->direct_link;
        $user->save();

        return response()->json([
            'message' => 'direct link add successfully'
        ]);
    }
}
