<?php

namespace App\Http\Controllers;

use App\BusinessLink;
use App\BusinessProfile;
use App\Http\Resources\OtherUserResource;
use App\Http\Resources\UserResource;
use App\Link;
use App\PersonalLinks;
use App\PersonalProfile;
use App\Services\FileManager;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserController extends Controller
{

    public function userProfile(Request $request, $username)
    {
        $user = User::with([
            'personalProfile',
            'personalProfile.personalLinks' ,
            'businessprofile',
            'businessprofile.businessLinks'
        ])
        ->where('username', $username)
        ->where('active', 1)
        ->first();

        if(is_null($user)) {
            return response()->json([
                'message' => __('message.sorry-no-result')
            ], 404);
        }

        if($user->is_public == false){
            return response()->json([
                'message' => 'sorry this account is private'
            ], 401);
        }
        $user->views += 1;
        $user->save();

        if($user->direct_link == true) {
            $profile = $user->current_profile == 'business' ? 'businessprofile' : 'personalProfile';
            $links = $user->current_profile == 'business' ? 'businessLinks' : 'personalLinks';

            $currentDirectLink = $user->{$profile}
                ->{$links}
                ->sortBy('sort')
                ->first();

            if(!($currentDirectLink && $currentDirectLink->link_url))
            {
                $user->direct_link = false;
            }

            if ($currentDirectLink) {
                return response()->json([
                    'direct_link' => $currentDirectLink->link_url ?? null,
                    'direct_link_mobile' => $currentDirectLink->link_url_mobile ?? null,
                ], 202);
            }
        }



        $record = new OtherUserResource($user);
        return response()->json($record);
    }
    // function for qr code

    // public function userProfileQrCode(Request $request, $username)
    // {

    // }

    public function myprofile(Request $request)
    {
        $user = \Auth::user();
        $records = new UserResource($user);
        return response()->json($records);
    }

    public function addNewAppFromScratch(Request $request,  FileManager $fileManager)
    {
       $data = $request->validate([
            'name_en' => 'required|string',
            'image'   => 'mimes:jpg,png,jpeg|nullable',
            'link_url'   =>  'required|string',
            'profile' => [
               'required',
                Rule::in(['personal', 'business'])
            ]
        ]);

        // create new app
        $app = new Link;
        $app->name_en = $data['name_en'];
        $app->name_ar = $data['name_en'];
        $app->prefix = '#^(.*)$#';
        $app->prefix_mobile = '#^(.*)$#';

        if ($request->hasFile('image') && request()->file('image')->isValid()) {
            $app->image = $fileManager->uploadLinkImage(request()->file('image'));
        }

        $app->user_id = \Auth::user()->id;
        $app->save();

        // create user link (personal or business)
        $profile = $data['profile'] == 'personal' ? PersonalProfile::class : BusinessProfile::class;
        $link = $data['profile'] == 'personal' ? new PersonalLinks : new BusinessLink;

        $profile = $profile::where('user_id', \Auth::user()->id)->first();

         $lastUserLink = $link::where($data['profile'] . '_profile_id', $profile->id)
            ->orderBy('sort', 'desc')
            ->first();

        $link->sort = is_null($lastUserLink) ? 1 : $lastUserLink->sort + 1;
        $link->{$data['profile'] . '_profile_id'} = $profile->id;
        $link->link_id = $app->id;
        $link->link_name = $app->name_en;
        $link->link_url = $data['link_url'];
        $link->save();

       return response()->json([
           'message' => __('message.added-successfully')
       ], 201);
   }
}
