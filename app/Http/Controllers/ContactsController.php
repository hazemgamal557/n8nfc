<?php

namespace App\Http\Controllers;

use App\Contacts;
use App\User;
use App\Http\Resources\ContactResource;
use App\Http\Resources\UserLocationResource;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    public function addFollower(Request $request, $id)
    {
        $data = $request->validate([
            'latitude'  => 'required|numeric',
            'longitude' => 'required|numeric'
        ]);
        $userExists = User::findOrFail($id);
        if($userExists->id == \Auth::user()->id)
        {
            return response()->json([
                'message' =>  __('message.sorry-you-cant-follow-your-self')
            ], 401);
        }
        // $checkFollowers = Contacts::where('user_id', \Auth::user()->id)
        //                 ->where('contact_id', $id)->first();
        // if(!empty($checkFollowers))
        // {

        //     return response()->json([
        //         'message' => __('message.sorry-it-was-added-before'),
        //     ], 401);
        // }

        if (!Contacts::where('user_id', \Auth::user()->id)->where('contact_id', $id)->count() > 0) {
            $contact  = new Contacts;
            $contact->user_id    = \Auth::user()->id;
            $contact->contact_id = $id;
            $contact->latitude   = $data['latitude'];
            $contact->longitude  = $data['longitude'];
            $contact->save();
        }

        // also add the auth user as a contact for the other user
        if (!Contacts::where('user_id', $id)->where('contact_id', \Auth::user()->id)->count() > 0) {
            $contact  = new Contacts;
            $contact->user_id    = $id;
            $contact->contact_id = \Auth::user()->id;
            $contact->latitude   = $data['latitude'];
            $contact->longitude  = $data['longitude'];
            $contact->save();
        }

        return response()->json([
            'message' => __('message.added-successfully'),
        ],201);
    }

    public function removeFollower(Request $request, $id)
    {
        $contacts = Contacts::where('user_id', \Auth::user()->id)->findOrFail($id);
        $contacts->delete();
        return response()->json([
            'message' => 'deleted successfully'
        ], 201);
    }

    public function allFollowres(Request $request)
    {
        $data = $request->validate([
            'q' => 'string|nullable'
        ]);

        $contacts = Contacts::with([
            'userFollowed',
            'userFollowed.personalProfile',
            'userFollowed.personalProfile.personalLinks',
            'userFollowed.businessprofile',
            'userFollowed.businessprofile.businessLinks'
        ])
        ->where('user_id', \Auth::user()->id)
        ->orderBy('id', 'desc')
        ->get();

        if(!empty($data['q'])) {
            $contacts = $contacts->filter(function ($contact) use ($data) {
                return (
                    strpos(
                        strtolower($contact->userFollowed->fullname),
                        strtolower($data['q'])
                    ) === 0
                    ||
                    strpos(
                        strtolower($contact->userFollowed->username),
                        strtolower($data['q'])
                    ) === 0
                );
            });
        }

        $contacts->transform(function ($contact) {
            $profile = $contact->userFollowed->current_profile == 'business' ? 'businessprofile' : 'personalProfile';
            $links = $contact->userFollowed->current_profile == 'business' ? 'businessLinks' : 'personalLinks';

            $directLink = $contact->userFollowed
                ->{$profile}
                ->{$links}
                ->sortBy('sort')
                ->first();

            $contact->userFollowed->current_direct_link = $directLink->link_url ?? null;
            $contact->userFollowed->current_direct_link_mobile = $directLink->link_url_mobile ?? null;

            if(is_null($contact->userFollowed->current_direct_link)){
                $contact->userFollowed->direct_link = false;
            }

            return $contact;
        });

        $records = ContactResource::collection($contacts);

        return response()->json([
            'contacts' => $records
        ]);
    }

    public function allFollowersLocation(Request $request)
    {
        $data = $request->validate([
            'q' => 'string|nullable'
        ]);

        $myFollowersLocation = Contacts::where('user_id', \Auth::user()->id)
          ->with('userFollowed')
          ->orderBy('id', 'desc')
          ->get();

          $records = UserLocationResource::collection($myFollowersLocation);
          return response()->json([
              'locations' => $records
          ]);
    }
}
