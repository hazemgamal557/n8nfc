<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::orderBy('id', 'DESC')
        ->paginate(10);
        
        return response()->json($users);

    }

    public function block($id)
    {
        $user = User::findOrFail($id);
        $user->active = false;
        $user->update();

        return response()->json([
            'message' => 'User has been blocked',
        ], 201);
    }

    public function unblock($id)
    {
        $user = User::findOrFail($id);
        $user->active = true;
        $user->update();

        return response()->json([
            'message' => 'User has been unblocked',
        ], 201);
    }


    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return response()->json([
            'message' => 'User has been deleted',
        ], 201);
    }

   
}
