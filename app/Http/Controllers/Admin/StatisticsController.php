<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatisticsController extends Controller
{
    /**
     * return how many client registered in the days
     * of the given month
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function usersStatistics(Request $request)
    {
        $data = $request->validate(['month' => 'required|date_format:Y-m']);

        $records = \App\User::where('active', 1)
            ->selectRaw("COUNT(id) AS total, DATE(created_at) AS month_day")
            ->whereYear('created_at', substr($data['month'], 0, 4))
            ->whereMonth('created_at', substr($data['month'], 5, 7))
            ->groupby('month_day')
            ->get();

        $currentDay = now()->parse("{$data['month']}-01");
        $daysLength = $currentDay->daysInMonth;

        $days = [];
        for ($day=1; $day <= $daysLength; $day++) {
            $dayString = $currentDay->toDateString();
            $days[] = [
                'date'  => $day,
                'count' => $records->where('month_day', $dayString)->first()->total ?? 0
            ];
            $currentDay->addDay();
        }

        return response()->json([
            'days' => $days
        ]);
    }
}
