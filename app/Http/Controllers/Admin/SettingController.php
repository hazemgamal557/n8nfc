<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
Use App\Setting;
Use App\User;
Use App\Collection;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function update(Request $request, $id)
    {
        $setting = Setting::findOrFail($id);

        $setting->content = $request->content;
        $setting->update();

        return response()->json([
            'message' => __('messages.setting-has-been-updated')
        ], 201);
    }
    public function retriveSocialMedia(Setting $setting)
    {
        $settings = Setting::where('key', 'LIKE', 'social_%')->get();

        return response()->json($settings);
        
    }
 
    
    public function getPage(string $key)
    {
        $key = "page_{$key}_" . app()->getLocale();


        $page = Setting::where('key', $key)
            ->firstOrFail()
            ->content;

        return response()->json([
            'content' => $page
        ]);
    }
    
    public function updatePage(Request $request, string $key)
    {
        $data = $request->validate(['content' => 'required|string']);

        $page = Setting::where('key', "page_{$key}_{$request->query('lang')}")
            ->update(['content' => $data['content']]);

        return response()->json([
            'message' => 'page has been updated successfully'
        ]);
    }

    public function count()
    {
        $users = User::count();
        $collections = Collection::count();
        return response()->json([
            'users'=> $users,
            'collections' => $collections
         ]);
    }


  
}
