<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\CollectionResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Collection;

class CollectionController extends Controller
{
    public function getAllCollection(Request $request)
    {
        $collections = Collection::orderBy('id', 'desc')->paginate($request->query('limit', 10));
        return response()->json($collections);
    }

    public function singleCollection(Request $request, $id)
    {
        $collection = Collection::findOrFail($id);
        return response()->json($collection);
    }

    public function addCollection(Request $request)
    {
        $data = $request->validate([
            'name_ar' => 'required|string',
            'name_en' => 'required|string'
        ]);
        $collection = new Collection;
        $collection->name_ar = $data['name_ar'];
        $collection->name_en = $data['name_en'];
        $collection->save();
        return response()->json([
            'message' => 'one collection added for your table'
        ], 201);
    }

    public function updateCollection(Request $request, $id)
    {
        $data = $request->validate([
            'name_ar' => 'string|nullable',
            'name_en' => 'string|nullable'
        ]);
        $collection = Collection::findOrFail($id);
        if(!empty($data['name_ar']))
        {
            $collection->name_ar = $data['name_ar'];
        }
        if(!empty($data['name_en']))
        {
            $collection->name_en = $data['name_en'];
        }
        $collection->save();
        return response()->json([
            'message' => 'update collection successfully'
        ], 201);

    }

    public function deleteCollection(Collection $collection)
    {
        $collection = $collection->delete();
        return response()->json([
            'message' => 'one row deleted'
        ], 201);
    }

}
