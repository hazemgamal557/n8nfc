<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Link;
use App\Services\FileManager;
use Illuminate\Http\Request;

class LinkController extends Controller
{

    public function links(Request $request, $id)
    {
        $links = Link::where('collection_id', $id)
            ->get();

        return ([
            'links' => $links,
        ]);
    }

    public function AddLink(Request $request, FileManager $fileManager)
    {
        $data = $request->validate([
            'name_ar' => 'required|string',
            'name_en' => 'required|string',
            'prefix'  => 'string',
            'prefix_mobile' => 'string',
            'image' => 'image|mimes:jpeg,jpg,png',
            'collection_id' => 'required|integer|exists:collections,id',
        ]);

        $links = new Link;
        $links->name_ar = $data['name_ar'];
        $links->name_en = $data['name_en'];

        $links->prefix = $data['prefix'] ?? '#^(.*)$#';
        $links->prefix_mobile = $data['prefix_mobile'] ?? $links->prefix;

        if ($request->hasFile('image') && request()->file('image')->isValid()) {
            $links->image = $fileManager->uploadLinkImage(request()->file('image'));
        }

        $links->collection_id = $data['collection_id'];
        $links->save();
        return response()->json([
            'message' => 'this link is created successfully',
        ], 201);
    }

    public function updateLink(Request $request, FileManager $fileManager, $id)
    {
        $data = $request->validate([
            'name_en' => 'string|nullable',
            'name_ar' => 'string|nullable',
            'prefix'  => 'string|nullable',
            'prefix_mobile'  => 'string|nullable',
            'image'   => 'image|mimes:jpeg,jpg,png',
        ]);

        $link = Link::findOrFail($id);
        if (!empty($data['name_en'])) {
            $link->name_en = $data['name_en'];
        }
        if (!empty($data['name_ar'])) {
            $link->name_ar = $data['name_ar'];
        }
        if (!empty($data['prefix'])) {
            $link->prefix = $data['prefix'];
        }
        if (!empty($data['prefix_mobile'])) {
            $link->prefix_mobile = $data['prefix_mobile'];
        }

        if ($request->hasFile('image') && request()->file('image')->isValid()) {
            $link->image = $fileManager->uploadLinkImage(request()->file('image'));
        }


        $link->save();

        return response()->json([
            'message' => 'your link updated successfully',
        ], 201);

    }

    public function DeleteLink(Link $link)
    {
        $link = $link->delete();
        return response()->json([
            'message' => 'link has been deleted successfully',
        ], 201);
    }
}
