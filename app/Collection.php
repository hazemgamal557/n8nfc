<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $fillable = [
        'name_ar',
        'name_en',
    ];
    public function links()
    {
        return $this->hasMany(Link::class, 'collection_id');
    }
}
