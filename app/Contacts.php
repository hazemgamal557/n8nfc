<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
     protected $fillable = ['user_id', 'contact_id', 'latitude', 'longitude'];
     
     public function userFollowed()
     {
         return $this->belongsTo(User::class, 'contact_id');
     }
}
