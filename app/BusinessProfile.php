<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessProfile extends Model
{
    protected $fillable = ['user_id', 'bio'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function businessLinks()
    {
        return $this->hasMany(BusinessLink::class, 'business_profile_id');
    }
}
