<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = [
        'name_ar',
        'name_en',
        'prefix',
        'prefix_mobile',
        'image',
        'collection_id',
        'user_id'
    ];
    public function collection()
    {
        return $this->belongsTo(Collection::class);
    }

    public function personalLinks()
    {
        return $this->hasMany(PersonalLinks::class, 'link_id');
    }

    public function businessLinks()
    {
        return $this->hasMany(BusinessLink::class, 'link_id');
    }

    public function getImageAttribute()
    {
        if (empty($this->attributes['image'])) {
            return asset('storage/default/link/default.png');
        }else{
            return asset("storage/{$this->attributes['image']}");
        }
    }
}
