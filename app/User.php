<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'email', 'username','facebook_id', 'active','image', 'gender', 'is_public', 'current_profile', 'birthday',
        'direct_link', 'profile_link'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    
    public function getImageAttribute()
    {
        if (empty($this->attributes['image'])) {
            return asset('storage/default/user_temp/default.png');
        } else {
            return asset("storage/{$this->attributes['image']}");
        }
    }
    public function personalProfile()
    {
        return $this->hasOne(PersonalProfile::class, 'user_id');
    }

    public function businessprofile()
    {
        return $this->hasOne(BusinessProfile::class, 'user_id');
    }

    public function socialMedia()
    {
        return $this->hasOne(SocialMedia::class, 'user_id');
    }
}
