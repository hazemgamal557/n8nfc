<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileManager
{

    public function uploadUserImage(UploadedFile $uploadedFile, string $prevFileName = '')
    {
       return $this->uploadFixedSizeImage($uploadedFile, $prevFileName, 'user', 'users', 512, 512);
    }

    public function uploadLinkImage(UploadedFile $uploadedFile, string $prevFileName = '')
    {
       return $this->uploadFixedSizeImage($uploadedFile, $prevFileName, 'link', 'links', 512, 512);
    }


    private function uploadFixedSizeImage(UploadedFile $uploadedFile, string $prevFileName, string $prefix, string $directory, int $width = 300, int $height = 300)
    {
        // delete the previous file if exists.
        if (!empty($prevFileName) && is_file( storage_path("app/public/{$prevFileName}") )) {
           $this->remove($prevFileName);
        }

        // generate the new file name.
        $newFileName =
            "{$prefix}-"
            . uniqid(time() . '-', true)
            . substr(pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME), 0, 10)
            . '.'
            . $uploadedFile->getClientOriginalExtension();

        // prepare the image file size
        $streamFileImage = \Image::make($uploadedFile)->fit($width, $height)->stream();

        // store the file
        Storage::put("public/{$directory}/{$newFileName}", $streamFileImage);
        return "{$directory}/$newFileName";
    }

    public function remove(string $file)
    {
        Storage::delete("public/{$file}");
    }

}
