<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonalProfile extends Model
{
    protected $fillable = ['user_id', 'bio'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function personalLinks()
    {
        return $this->hasMany(PersonalLinks::class, 'personal_profile_id');
    }

}
